<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')
	->name('index');
Route::get('/search', 'ProductsController@search')
	->name('search');

Route::match(['get', 'post'], '/profile/{id?}', 'ProfileController@profile')
	->name('profile');

/**
 * Products
 */
Route::group(['prefix' => 'product'], function() {
	Route::get('/', function() {
		return redirect(url('/'));
	});

	Route::get('detail/{id?}', 'ProductsController@detail')
		->name('product/detail');

	Route::get('brand/{brand?}', 'ProductsController@brand')
		->name('product/brand');
});

/**
 * Contact
 */
Route::get('/contact', function() {
	return view('contact.index');
});

/**
 * Item
 */
Route::group(['prefix' => 'backend/item'], function() {
	/** List
	 */
	Route::get('/', 'ItemsController@index')->name('backend/item');

	/** Add
	 */
	Route::match(['post', 'get'], '/add', 'ItemsController@add')
		->name('backend/item/add');

	/** Edit
	 */
	Route::match(['post', 'get'], '/edit/{id?}', 'ItemsController@edit')
		->name('backend/item/edit');
});

/**
 * Brand
 */
Route::group(['prefix' => 'backend/brand'], function () {
	/** List
	 */
	Route::get('/', 'BrandsController@index')
		->name('backend/brand');

	/** Add
	 */
	Route::match(['post', 'get'], '/add', 'BrandsController@add')
		->name('backend/brand/add');

	/** Edit
	 */
	Route::match(['post', 'get'], '/edit/{id?}', 'BrandsController@edit')
		->name('backend/brand/edit');

	/** Delete
	 */
	Route::get('/delete/{id?}', 'BrandsController@delete')
		->name('backend/brand/delete');
});

/**
 * Media
 */
Route::group(['prefix' => 'file'], function () {
	/** For file
	 */
	Route::get('/images/{filename}', function($filename) {
		/** path to storage/app/uploads
		 */
		$path = 'uploads/' . $filename;

		if (!Storage::exists($path)) 
			abort(404);

		$file = Storage::get($path);
		$type = Storage::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	});
});
