	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>e</span>-galery</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="companyinfo">
							<form action="#" class="input-group input-group-lg">
								<input style="border-radius: 0;" class="form-control" type="text" placeholder="Your email address" />
								<span class="input-group-btn">
									<button style="border-radius: 0;" type="submit" class="btn btn-warning"><i class="fa fa-arrow-circle-o-right"></i></button>
								</span>
							</form>
							<br>
							<p>Get the most recent updates from our site and be updated your self...</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="{{ asset('frontend/images/home/map.png') }}" alt="" />
							<p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-3">
						<div class="single-widget">
							<h2>Service Center</h2>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Galery</h2>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="single-widget">
							<h2>About Galery</h2>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2017 E-Galery Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
	<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
	<script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>