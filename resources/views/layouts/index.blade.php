<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('partials.header')
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@egalery.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="{{ asset('frontend/images/home/logo.png') }}" alt="" /></a>
						</div>
						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									en-US
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">en-US</a></li>
									<li><a href="#">en-UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									US Dollar
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">US Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							@if (Auth::guest())
								<ul class="nav navbar-nav">
									<li><a href="{{ route('register') }}"><i class="fa fa-user"></i> Register</a></li>
									<li><a href="{{ route('login') }}"><i class="fa fa-lock"></i> Sign in</a></li>
								</ul>
							@else
								<ul class="nav navbar-nav">
									<li><a href="{{ route('backend/item') }}"><i class="fa fa-crosshairs"></i> Items</a></li>
									<li><a href="{{ route('backend/brand') }}"><i class="fa fa-star"></i> Brand</a></li>
									<li><a href="#"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a></li>
									<li>
										<a href="{{ route('logout') }}" 
											onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
											<i class="fa fa-lock"></i> Logout 
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
									</li>
								</ul>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom" style="background: #FE980F; margin-bottom: 30px;"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="{{ route('index') }}" class="active">Home</a></li>
								<li><a href="{{ url('404') }}">404</a></li>
								<li><a href="{{ url('contact') }}">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<form action="{{ url('search') }}" method="get">
								<input name="q" type="text" placeholder="Search"/>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	@yield('content')
	
	@include('partials.footer')
</body>
</html>