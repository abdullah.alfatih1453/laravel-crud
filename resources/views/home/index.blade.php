@extends('layouts.index')

@section('content')
	<section>
		<div class="container">
			<div class="row">

				@include('partials/sidebar')

				<div class="col-sm-9">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						@if ($items)
							@foreach($items as $item)
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{ url('file/images/' . $item->picture) }}" alt="{{ $item->name }}" />
												<h2>
													<strike>${{ number_format($item->cost) }}</strike>
												</h2>
												<p>{{ $item->name }}</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Discount {{ $item->discount }}%</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>${{ number_format($item->cost * ($item->discount / 100)) }}</h2>
													<p>{{ $item->name }}</p>
													<a href="{{ url('product/detail/' . $item->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-eye"></i>More Detail</a>
													<br>
													@if(Auth::user())
													<a href="{{ url('product/detail/' . $item->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-cogs"></i>Edit</a>
													@endif
												</div>
											</div>
									</div>
									<div class="choose">
										<ul class="nav nav-pills nav-justified">
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
							<br>
							{{ $items->links() }}
						@endif
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
@stop()