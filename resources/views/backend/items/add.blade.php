@extends('layouts.index')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb" style="margin-bottom: 25px">
				  <li><a href="#">Backend</a></li>
				  <li><a href="#">Items</a></li>
				  <li class="active">Add New</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<form method="post" enctype="multipart/form-data" class="shopper-informations">
				<div class="row">
					<div class="col-sm-12">
						@if ($errors->all())
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									{{ $error }} <br>
								@endforeach
							</div>
						@endif

						@if (session()->has('success'))
							<div class="alert alert-success">	
								{{ session()->get('success') }}
							</div>
						@endif

						@if (session()->has('error'))
							<div class="alert alert-success">
								{{ session()->get('error') }}
							</div>
						@endif
					</div>
					<div class="col-sm-3">
						<div class="order-message"><!--shipping-->
							<p>Picture</p>
							<img class="img-responsive" src="{{ asset('frontend/images/home/shipping.jpg') }}" alt="" />
						</div><!--/shipping-->
					</div>
					<div class="col-sm-4 clearfix">
						<div class="bill-to">
							<p>Detail</p>
							<div class="form-two" style="width: 100%; margin: 0">
								{{ csrf_field() }}
								<input name="webid" type="text" placeholder="Web id" readonly value="{{ uniqid() }}">
								<input name="name" value="{{ old('name') }}" type="text" placeholder="Name of Item *">
								<input name="picture" type="file" name="picture" placeholder="Picture of item">
								<select name="brand" placeholder="brand">
									@foreach ($brands as $brand)
										<option value="{{ $brand->id }}">{{ $brand->name }}</option>
									@endforeach
								</select>
								<input name="cost" value="{{ old('cost') }}" type="number" placeholder="Cost">
								<input name="discount" value="{{ old('discount') }}" type="number" placeholder="Discount">
								<input name="qty" value="{{ old('qty') }}" type="number" placeholder="Qty">
							</div>
						</div>
						<div class="">
							<button class="btn btn-primary">Submit</button>
							<a href="{{ route('backend/item') }}" class="btn btn-primary">Cancel</a>
						</div>
						<br>
						<br>
					</div>
					<div class="col-sm-5">
						<div class="order-message">
							<p>Description</p>
							<textarea name="description"  placeholder="Notes about your order, Special Notes for Delivery" rows="16">{{ old('description') }}</textarea>
						</div>	
					</div>					
				</div>
			</form>
		</div>
	</section> <!--/#cart_items-->
@stop()