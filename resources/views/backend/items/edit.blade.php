@extends('layouts.index')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb" style="margin-bottom: 25px">
				  <li><a href="#">Backend</a></li>
				  <li><a href="{{ url('backend/item') }}">Brands</a></li>
				  <li class="active">Edit</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<form method="post" enctype="multipart/form-data" class="shopper-informations">
				<div class="row">
					<div class="col-sm-12">
						@if ($errors->all())
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									{{ $error }} <br>
								@endforeach
							</div>
						@endif

						@if (session()->has('success'))
							<div class="alert alert-success">	
								{{ session()->get('success') }}
							</div>
						@endif

						@if (session()->has('error'))
							<div class="alert alert-success">
								{{ session()->get('error') }}
							</div>
						@endif
					</div>
					<div class="col-sm-3">
						<div class="order-message"><!--shipping-->
							<p>Picture</p>
							<img class="img-responsive" src="{{ url('file/images') . '/' . $item->picture }}" alt="" />
						</div><!--/shipping-->
					</div>
					<div class="col-sm-4 clearfix">
						<div class="bill-to">
							<p>Detail</p>
							<div class="form-two" style="width: 100%; margin: 0">
								{{ csrf_field() }}
								<input name="webid" type="text" placeholder="Web id" readonly value="{{ $item->webid }}">
								<input name="name" value="{{ $item->name }}" type="text" placeholder="Name of Item *">
								<input name="picture" type="file" name="picture" placeholder="Picture of item">
								<select name="brand" placeholder="brand">
									@foreach ($brands as $brand)
										<option 
											@if ($item->brand_id == $brand->id)
												selected="selected"
											@endif
											value="{{ $brand->id }}">
											{{ $brand->name }}
										</option>
									@endforeach
								</select>
								<input name="cost" value="{{ $item->cost }}" type="number" placeholder="Cost">
								<input name="discount" value="{{ $item->discount }}" type="number" placeholder="Discount">
								<input name="qty" value="{{ $item->qty }}" type="number" placeholder="Qty">
							</div>
						</div>
						<div class="">
							<button class="btn btn-primary">Submit</button>
							<a href="{{ route('backend/item') }}" class="btn btn-primary">Cancel</a>
						</div>
						<br>
						<br>
					</div>
					<div class="col-sm-5">
						<div class="order-message">
							<p>Description</p>
							<textarea name="description"  placeholder="Notes about your order, Special Notes for Delivery" rows="16">{{ $item->description }}</textarea>
						</div>	
					</div>					
				</div>
			</form>
		</div>
	</section> <!--/#cart_items-->
@stop()