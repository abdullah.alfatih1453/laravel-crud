@extends('layouts.index')

@section('content')	
	<section id="cart_items">
		<div class="container">
			<div class="row">

				@include('partials.sidebar')

				<div class="col-sm-9">
					<div class="breadcrumbs">
						<ol class="breadcrumb" style="margin-bottom: 25px">
						  <li><a href="#">Backend</a></li>
						  <li class="active">Items</li>
						</ol>
					</div>
					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="image">Item</td>
									<td class="description"></td>
									<td class="price">Price</td>
									<td class="quantity">Discount</td>
									<td class="total">Total</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								@if (count($items) > 0)
									@foreach ($items as $item)
									<tr>
										<td class="">
											<a href=""><img class="img-responsive" width="80" src="{{ url('file/images') . '/' . $item->picture }}" alt=""></a>
										</td>
										<td class="">
											<h4><a href="{{ url('backend/item/edit/'. $item->id) }}">{{ $item->name }}</a></h4>
											<p>Web ID: {{ $item->webid }}</p>
										</td>
										<td class="">
											<p>${{ number_format($item->cost) }}</p>
										</td>
										<td class="">
											<p>-{{ $item->discount }}%</p>
										</td>
										<td class="cart_total">
											<p class="cart_total_price">${{ number_format($item->cost * ($item->discount / 100)) }}</p>
										</td>
										<td class="cart_delete" style="margin-top: 15px">
											<a class="cart_quantity_delete" href="{{ url('backend/item/edit/'. $item->id) }}"><i class="fa fa-eye"></i></a>
											<a class="cart_quantity_delete" href="{{ url('backend/item/delete/'. $item->id) }}"><i class="fa fa-times"></i></a>
										</td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
						<br>
						{{ $items->links() }}
					</div>
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->
@stop