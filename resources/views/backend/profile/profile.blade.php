@extends('layouts.index')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb" style="margin-bottom: 25px">
				  <li><a href="#">Backend</a></li>
				  <li><a href="javascript:">Preference</a></li>
				  <li class="active">Profile</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<form method="post" enctype="multipart/form-data" class="shopper-informations">
				<div class="row">
					<div class="col-sm-12">
						@if ($errors->all())
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									{{ $error }} <br>
								@endforeach
							</div>
						@endif

						@if (session()->has('success'))
							<div class="alert alert-success">	
								{{ session()->get('success') }}
							</div>
						@endif

						@if (session()->has('error'))
							<div class="alert alert-success">
								{{ session()->get('error') }}
							</div>
						@endif
					</div>
					<div class="col-sm-4 clearfix">
						<div class="bill-to">
							<p>Detail</p>
							<div class="form-two" style="width: 100%; margin: 0">
								{{ csrf_field() }}
								<input name="id" type="text" placeholder="Web id" disabled value="{{ $user->id }}">
								<input name="email" type="text" placeholder="Web id" disabled value="{{ $user->email }}">
								<input name="name" value="{{ $user->name }}" type="text" placeholder="Name of Item *">
								<input name="newPassword" value="" type="text" placeholder="Your Password">
							</div>
						</div>
						<div class="">
							<button class="btn btn-primary">Submit</button>
							<a href="{{ route('backend/item') }}" class="btn btn-primary">Cancel</a>
						</div>
						<br>
						<br>
					</div>				
				</div>
			</form>
		</div>
	</section> <!--/#cart_items-->
@stop()