@extends('layouts.index')

@section('content')	
	<section id="cart_items">
		<div class="container">
			<div class="row">

				@include('partials.sidebar')

				<div class="col-sm-9">
					<div class="breadcrumbs">
						<ol class="breadcrumb" style="margin-bottom: 25px">
						  <li><a href="#">Backend</a></li>
						  <li class="active">Brand</li>
						</ol>
					</div>
					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="description">Name</td>
									<td class="price">slug</td>
									<td class="quantity">Description</td>
									<td class="total">Create At</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								@if (count($brands) > 0)
									@foreach ($brands as $item)
									<tr>
										<td class="cart_description">
											<h4><a href="{{ url('backend/brand/edit/' . $item->id) }}">{{ $item->name }}</a></h4>
											<p>Web ID: {{ $item->id }}</p>
										</td>
										<td class="cart_price">
											<p>{{ $item->slug }}</p>
										</td>
										<td class="cart_price">
											<p>{{ $item->description }}</p>
										</td>
										<td class="">
											<p class="">{{ $item->created_at }}</p>
										</td>
										<td class="cart_delete" style="margin-top: 15px;">
											<a class="cart_quantity_delete" href="{{ url('backend/brand/edit/' . $item->id) }}"><i class="fa fa-eye"></i></a>
											<a class="cart_quantity_delete" href="{{ url('backend/brand/delete/' . $item->id) }}"><i class="fa fa-times"></i></a>
										</td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
						<br>
						{{ $brands->links() }}
					</div>
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<div>
	</div>
@stop