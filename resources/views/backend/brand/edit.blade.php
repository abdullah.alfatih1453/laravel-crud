@extends('layouts.index')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb" style="margin-bottom: 25px">
				  <li><a href="#">Backend</a></li>
				  <li><a href="{{ route('backend/brand') }}">Items</a></li>
				  <li class="active">Edit</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<form method="post" class="shopper-informations">
				<div class="row">
					<div class="col-sm-12">
						@if ($errors->all())
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									{{ $error }} <br>
								@endforeach
							</div>
						@endif

						@if (session()->has('success'))
							<div class="alert alert-success">	
								{{ session()->get('success') }}
							</div>
						@endif

						@if (session()->has('error'))
							<div class="alert alert-success">
								{{ session()->get('error') }}
							</div>
						@endif
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Detail</p>
							<div class="form-two" style="width: 100%; margin: 0">
								{{ csrf_field() }}
								<input name="name" type="text" placeholder="Name *" value="{{ $brand->name }}">
								<input name="slug" type="text" placeholder="Slug *" value="{{ $brand->slug }}">
							</div>
						</div>
						<div class="">
							<button class="btn btn-primary" type="submit">Submit</button>
							<a href="{{ route('backend/item') }}" class="btn btn-primary">Cancel</a>
						</div>
						<br>
						<br>
					</div>
					<div class="col-sm-5">
						<div class="order-message">
							<p>Description</p>
							<textarea name="description" placeholder="Notes about your order, Special Notes for Delivery" rows="16">{{ $brand->description }}</textarea>
						</div>	
					</div>				
				</div>
			</form>
		</div>
	</section> <!--/#cart_items-->
@stop()