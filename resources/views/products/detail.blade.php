@extends('layouts/index')

@section('content')
	<section>
		<div class="container">
			<div class="row">
				@include('partials/sidebar');
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img class="img-responsive" src="{{ url('file/images/' . $item->picture) }}" alt="" />
								<h3>ZOOM</h3>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<h2>{{ $item->name }}</h2>
								<p>Web ID: {{ $item->webid }}</p>
								<img src="{{ asset('frontend/images/product-details/rating.png') }}" alt="" />
								<br>
								<span>
									<strike>${{ number_format($item->cost) }}</strike> / {{ $item->discount }}%<br>
									<span>US ${{ number_format($item->cost * ($item->discount / 100)) }}</span>
									<label>Stock:</label>
									<input type="text" value="{{ $item->qty }}" />
								</span>
								<p><b>Availability:</b> @if($item->qty > 0) In Stock @else Out of Stock @endif</p>
								<p><b>Condition:</b> New</p>
								<p><b>Brand:</b> {{ $itemBrand->name }}</p>
								<a href=""><img src="{{ asset('frontend/images/product-details/share.png') }}" class="share img-responsive"  alt="" /></a>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#reviews" data-toggle="tab">Details</a></li>
							</ul>
						</div>
						<div class="tab-content">
							
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12">
									<ul>
										<li><a href=""><i class="fa fa-user"></i>Admin</a></li>
										<li><a href=""><i class="fa fa-calendar-o"></i>{{ $item->created_at }}</a></li>
									</ul>
									<p>{{ $item->description }}</p>
									
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
				</div>
			</div>
		</div>
	</section>
@stop