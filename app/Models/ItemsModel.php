<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemsModel extends Model
{
    /** Table
     */
   protected $table = 'items';

   /** Fillable
    */
   protected $fillable = [
	   	'webid', 
	   	'name', 
	   	'description', 
	   	'picture', 
	   	'cost', 
	   	'discount', 
	   	'qty', 
	   	'brand_id'
   	];
}
