<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandsModel extends Model
{
    /** Table
     */
   protected $table = 'brands';

   /** Fillable
    */
   protected $fillable = [
	   'name', 
	   'slug', 
	   'description'
   ];
}
