<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BrandsModel;
use App\Models\ItemsModel;

class ProductsController extends Controller
{
    public function detail(Request $request)
    {
    	$id = $request->id;

 		if (!$id)
 			return redirect(url('/'));

    	$brand = new BrandsModel;
    	$data['brands'] = $brand->all();
    	$data['item'] = ItemsModel::findOrFail($id);
    	$data['itemBrand'] = $brand->find($data['item']->brand_id);

    	return view('products.detail', $data);
    }

    public function brand(Request $request)
    {
    	$var = $request->brand;

    	$brand = new BrandsModel;
    	$b = $brand->where('slug', 'like', "%$var%")->firstOrFail();

    	$data['items'] = ItemsModel::where('brand_id', "$b->id")->paginate(15);
    	$data['brands'] = $brand->all();

    	return view('home.index', $data);
    }

    public function search(Request $request)
    {
    	$qs = $request->input('q');

    	$data['brands'] = BrandsModel::all();
    	$data['items'] = ItemsModel::where('name', 'like', "%$qs%")
    	->paginate(100);

    	return view('home.index', $data);
    }
}
