<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemsModel;
use App\Models\BrandsModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['items'] = ItemsModel::paginate(15);
        $data['brands'] = BrandsModel::all();

        return view('home/index', $data);
    }
}
