<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BrandsModel;

class BrandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	/** 
	 * Show list of brands
	 *
	 * @param -
	 * @return view
	 */
    public function index()
    {
    	$brand = new BrandsModel;
        $data['brands'] = $brand->paginate(10);

        return view('backend.brand.index', $data);
    }

    /** 
	 * Add a new record
	 *
	 * @param Request
	 * @return form
	 */
    public function add(Request $request)
    {
        /** Block for unsigned user
         */
    	//$this->middleware('auth');

    	if ($request->isMethod('post')) {
    		/** Validation Data
    		 */
    		$validator = $this->validator($request->all());

    		if ($validator->fails()) {
    			return redirect('backend/brand/add')
                        ->withErrors($validator)
                        ->withInput();
    		}

            $brand = new BrandsModel;

            $brand->name        = $request->input('name');
            $brand->slug        = strtolower($request->input('slug'));
            $brand->description = $request->input('description');

            $save = $brand->save();

            /** if was saved
             */
            if ($save) {
                $request->session()->flash('success', 'Data has been save to database');
                return redirect('backend/brand/add');
            } else {
                $request->session()->flash('error', 'Opps!! Something look wrong');
                return redirect('backend/brand/add');
            }
    	}

        return view('backend/brand/add');
    }

    /** 
     * Edit record
     *
     * @param Request
     * @return form
     */
    public function edit(Request $request)
    {
        /** Block for unsigned user
         */
        //$this->middleware('auth');

        $id = $request->id;

        if (empty($id) || !$id) {
            $request->session()->flash('error', 'We cant find it!! lets add new once');
            return redirect('backend/brand/add');
        }

        $brand = new BrandsModel;
        $data = $brand->find($id);
        $var['brand'] = $data;

        if ($request->isMethod('post')) {
            /** Validation Data
             */
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                return redirect('backend/brand/add')
                        ->withErrors($validator)
                        ->withInput();
            }

            /** Edit data
             */
            $data->name = $request->input('name');
            $data->slug = strtolower($request->input('slug'));
            $data->description = $request->input('description');

            $save = $data->save();

            /** if was saved
             */
            if ($save) {
                $request->session()->flash('success', 'Data has been save to database');
                return redirect('backend/brand/edit/'.$id);
            } else {
                $request->session()->flash('error', 'Opps!! Something look wrong');
                return redirect('backend/brand/edit/'.$id);
            }
        }

        return view('backend/brand/edit', $var);
    }

    /** 
     * Edit record
     *
     * @param Request
     * @return form
     */
    public function delete(Request $request)
    {
        /** Block for unsigned user
         */
        //$this->middleware('auth');

        $id = $request->id;

        if (empty($id) || !$id) {
            $request->session()->flash('error', 'We cant find it!! lets add new once');
            return redirect('backend/brand/add');
        }

        $data = BrandsModel::findOrFail($id);
        $del = $data->delete();

        /** if was saved
         */
        if ($del) {
            $request->session()->flash('success', 'Data has been delete from database');
            return redirect('backend/brand/');
        } else {
            $request->session()->flash('error', 'Opps!! Something look wrong');
            return redirect('backend/brand/');
        }
    }

    /** 
	 * Validation rules
	 *
	 * @param Request::all()
	 * @return validation object
	 */
    protected function validator(array $data)
    {
    	return Validator::make($data, [
    		'name' => 'required|min:1|max:50',
    		'slug' => 'required|min:1|max:25',
    		'description' => 'min:1'
    	]);
    }
}
