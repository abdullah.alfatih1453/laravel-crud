<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ItemsModel;
use App\Models\BrandsModel;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	/** 
	 * Show list of brands
	 *
	 * @param -
	 * @return view
	 */
    public function index()
    {
    	$item = new ItemsModel;
        $data['items'] = $item->paginate(10);
        $data['brands'] = BrandsModel::all();

        return view('backend.items.index', $data);
    }

    /** 
	 * Add a new record
	 *
	 * @param Request
	 * @return form
	 */
    public function add(Request $request)
    {
        /** Block for unsigned user
         */
    	//$this->middleware('auth');

    	$brand = new BrandsModel;
    	$data['brands'] = $brand->all();

    	if ($request->isMethod('post')) {
    		/** Validation Data
    		 */
    		$validator = $this->validator($request->all());

    		if ($validator->fails()) {
    			return redirect('backend/item/add')
                        ->withErrors($validator)
                        ->withInput();
    		}

    		/** Get the file tmp
    		 */
    		$file = $request->file('picture');
    		$file_name = uniqid();
    		$file->storeAs('uploads', $file_name . '.' . $file->extension());

    		/** Store data to database
    		 */
    		$item = new ItemsModel;
    		$item->webid 		= $request->input('webid');
    		$item->name 		= $request->input('name');
    		$item->description 	= $request->input('description');
    		$item->cost 		= $request->input('cost');
    		$item->discount 	= $request->input('discount');
    		$item->qty 			= $request->input('qty');
    		$item->brand_id 	= $request->input('brand');
    		$item->picture		= $file_name . '.' . $file->extension();

    		$save = $item->save();

            /** if was saved
             */
            if ($save) {
                $request->session()->flash('success', 'Data has been save to database');
                return redirect('backend/item/add');
            } else {
                $request->session()->flash('error', 'Opps!! Something look wrong');
                return redirect('backend/item/add');
            }
    	}

        return view('backend/items/add', $data);
    }

    /** 
     * Edit record
     *
     * @param Request
     * @return form
     */
    public function edit(Request $request)
    {
        /** Block for unsigned user
         */
        //$this->middleware('auth');

        $id = $request->id;

        if (empty($id) || !$id) {
            $request->session()->flash('error', 'We cant find it!! lets add new once');
            return redirect('backend/item/add');
        }

        $brand = new BrandsModel;
        $var['brands'] = $brand->all();

        $item = new ItemsModel;
        $data = $item->findOrFail($id);
        $var['item'] = $data;

        if ($request->isMethod('post')) {
            /** Validation Data
             */
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                return redirect('backend/item/edit/' . $data->id)
                        ->withErrors($validator)
                        ->withInput();
            }

            /** If has file
    		 */
            if ($request->hasFile('picture')) {
            	/** upload and rename the file
            	 */
	    		$file = $request->file('picture');
	    		$file_name = uniqid();
	    		$file->storeAs('uploads', $file_name . '.' . $file->extension());

	    		/** set to database
	    		 */
	    		$data->picture = $file_name . '.' . $file->extension();
            }

    		/** Store data to database
    		 */
    		$data->webid 		= $request->input('webid');
    		$data->name 		= $request->input('name');
    		$data->description 	= $request->input('description');
    		$data->cost 		= $request->input('cost');
    		$data->discount 	= $request->input('discount');
    		$data->qty 			= $request->input('qty');
    		$data->brand_id 	= $request->input('brand');

    		$save = $data->save();

            /** if was saved
             */
            if ($save) {
                $request->session()->flash('success', 'Data has been save to database');
                return redirect('backend/item/edit/'.$id);
            } else {
                $request->session()->flash('error', 'Opps!! Something look wrong');
                return redirect('backend/item/edit/'.$id);
            }
        }

        return view('backend/items/edit', $var);
    }

    /** 
     * Edit record
     *
     * @param Request
     * @return form
     */
    public function delete(Request $request)
    {
        /** Block for unsigned user
         */
        //$this->middleware('auth');

        $id = $request->id;

        if (empty($id) || !$id) {
            $request->session()->flash('error', 'We cant find it!! lets add new once');
            return redirect('backend/item/add');
        }

        $data = BrandsModel::findOrFail($id);
        $del = $data->delete();

        /** if was saved
         */
        if ($del) {
            $request->session()->flash('success', 'Data has been delete from database');
            return redirect('backend/item/');
        } else {
            $request->session()->flash('error', 'Opps!! Something look wrong');
            return redirect('backend/item/');
        }
    }

    /** 
	 * Validation rules
	 *
	 * @param Request::all()
	 * @return validation object
	 */
    protected function validator(array $data)
    {
    	return Validator::make($data, [
    		'webid' 		=> 'required|alpha_num',
    		'name' 			=> 'required|min:1|max:50',
    		'description' 	=> 'required|min:1',
    		'cost' 			=> 'required|numeric',
    		'discount' 		=> 'required|numeric|min:1|max:100',
    		'qty' 			=> 'required|min:1|max:5',
    		'brand'			=> 'required',
    		//'picture'		=> 'required|image'
    	]);
    }
}
