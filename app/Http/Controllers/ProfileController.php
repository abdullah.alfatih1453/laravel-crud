<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class ProfileController extends Controller
{
    public function profile(Request $request)
    {
    	$id = $request->id;

        if (empty($id) || !$id) {
            $request->session()->flash('error', 'We cant find it!! lets add new once');
            return redirect('backend/item/add');
        }

        $user = User::findOrFail($id);
        $data['user'] = $user;

        if ($request->isMethod('post')) {
        	/** Validation Data
             */
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                return redirect('profile/' . $user->id)
                        ->withErrors($validator)
                        ->withInput();
            }

        	if ($request->has('newPassword')) {
        		$user->password = bcrypt($request->input('newPassword'));
        	}

        	$user->name = $request->input('name');
        	$save = $user->save();

        	/** if was saved
             */
            if ($save) {
                $request->session()->flash('success', 'Data has been save to database');
                return redirect('profile/' . $user->id);
            } else {
                $request->session()->flash('error', 'Opps!! Something look wrong');
                return redirect('profile/' . $user->id);
            }
        }

    	return view('backend.profile.profile', $data);
    }

    /** 
	 * Validation rules
	 *
	 * @param Request::all()
	 * @return validation object
	 */
    protected function validator(array $data)
    {
    	return Validator::make($data, [
    		'name' => 'min:1|max:199'
    		//'picture'		=> 'required|image'
    	]);
    }
}
